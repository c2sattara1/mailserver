package mailserver;

import java.net.*;
import java.io.*;
import java.util.*;
/**
 * Open an SMTP connection to a mailserver and send one mail.
 *
 */
public class SMTPConnection {
	/* The socket to the server */
	private Socket connection;
	
	/* Initialise all Streams for reading and writing the socket */
	private BufferedReader fromServer;
	private DataOutputStream toServer;
	private static final int SMTP_PORT = 25;
	private static final String CRLF = "\r\n";
	
	/* Are we connected? Used in close() to determine what to do. */
	private boolean isConnected = false;
	
	/* Create an SMTPConnection object. Create the socket and the
associated streams. Initialise SMTP connection. */
	public SMTPConnection(Envelope envelope) throws IOException {
		//Initialise the connection
		connection = new Socket(envelope.DestAddr,SMTP_PORT);
		//Initialise the other variables
		fromServer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		toServer = new DataOutputStream(connection.getOutputStream());
		
		/* Read a line from server and check that the reply code is 220.
		If not, throw an IOException. */
		String requestLine;
		requestLine = fromServer.readLine();
		
		//Check that the reply code is 220(HELO)
		int checkcode = parseReply(requestLine);
		if(checkcode != 220)
			throw new IOException();
			
		/* SMTP handshake. We need the name of the local machine.
Send the appropriate SMTP handshake command. */
		/* local host be ourself*/
		String localhost = InetAddress.getLocalHost().getHostName();
		sendCommand("HELO "+localhost+CRLF,250);//sendCommand accepts the string and the code
		isConnected = true;
	}
	
	/* Send the message. Write the correct SMTP-commands in the
correct order. No checking for errors, just throw them to the
caller. */
	public void send(Envelope envelope) throws IOException {
		/* Fill in */
		/* Send all the necessary commands to send a message. CallsendCommand() to do the dirty work. Do _not_ catch the exception thrown from sendCommand(). */
		sendCommand("MAIL FROM: "+"<"+envelope.Sender+">"+CRLF,250);//check this should there be a CRLF
		sendCommand("RCPT TO: " +"<"+envelope.Recipient+">"+CRLF,250);
		sendCommand("DATA"+CRLF,354);
		// now as we know it terminates a "."
		String dot = ".";
		sendCommand(envelope.Message+CRLF+dot+CRLF,250);
	}
	/* Send an SMTP command to the server. Check that the reply code is
	what is is supposed to be according to RFC 821. */
	private void sendCommand(String command, int rc) throws IOException {
			/* Write command to server and read reply from server. */
			String rep;
			//write the command on the server
			toServer.writeBytes(command);
			rep = fromServer.readLine();
			System.out.println("Rely: " + rep );
			/* Check that the server's reply code is the same as the parameter
	rc. If not, throw an IOException. */
			int code_new = parseReply(rep);
			if(code_new != rc)
			throw new IOException();

	}
	
/* Close the connection. First, terminate on SMTP level, then
A Mail User Agent in Java http://media.pearsoncmg.com/aw/aw_kurose_network_3/labs/lab2/lab2.html
4 of 5 9/9/2010 11:26 AM
close the socket. */
	public void close() throws IOException {
		isConnected = false;
		try {
			sendCommand("QUIT"+CRLF , 221);
			connection.close();
		} catch (IOException e) {
			System.out.println("Unable to close connection: " + e);
			isConnected = true;
		}
	}

	/* Parse the reply line from the server. Returns the reply code. */
	private int parseReply(String reply) {
		String create = null;
		StringTokenizer tokens = new StringTokenizer(reply);
		if(tokens.hasMoreTokens()){
			 create = tokens.nextToken();
		}
		//using the parse Int  function 
		int num = Integer.parseInt(create);
		return num;
	}


	/* Destructor. Closes the connection if something bad happens. */
	protected void finalize() throws Throwable {
		if(isConnected) {
			close();
		}
		super.finalize();
	}
}

